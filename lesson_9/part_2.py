name = "Rar"


class SwimMixin:

    # def __new__(cls, *args, **kwargs):
    #     raise Exception("Don't do this!")

    def swim(self):
        print(f"{self.name} swimming now!")


class Cat(object):  # in other file plz, all in class is "interface"
    name = "Tor"  # свойство (атрибут)
    color = "Black"

    def __init__(self, name, color=None):
        self.name = name
        if color:
            self.color = color

    def __str__(self):
        return f"{self.color, self.name}"

    def walk(self):
        print(f"{self.name} is walking now")

    def __del__(self):
        print(F"Cat {self.name} dead now")


class MegaCat(Cat):
    age = 22

    def __init__(self, *args, **kwargs):
        self.age = kwargs.pop("age", 0)
        super(MegaCat, self).__init__(*args, **kwargs)

    def walk(self):
        super(MegaCat, self).walk()
        print("Walk as cool cat!")


class Bird:
    name = "Kesha"

    def fly(self):
        print(f"{self.name} is fling now!")


class CatBird(Bird, SwimMixin, Cat):
    def __del__(self):
        pass


catbird = CatBird(name="Fenrir", color="Red")

catbird.walk()
catbird.fly()


cat1 = Cat(name="Tor", color="Black")  # инстанс (рабочий обект)
# del cat1
cat2 = Cat(name="Izum", color="Ginger")
cat3 = MegaCat(name="Magnus", color="White")

cat3.walk()

# cat6 = CatBird()

# cat6.swim()

cat1.walk()

# cat1 = Cat()
# cat2 = Cat()
#
# cat2.name = "Izum"
# cat2.color = "Ginger"

# print(cat1.name, cat2.name)
# print(cat1.color, cat2.color)
# cat2 = Cat  # объект класса



