from functools import reduce

some_list = [1, 2, 3, 5, 7]

# some_list = [x + 1 for x in some_list]

# some_list = map(lambda x: x + 1, some_list)  # return generator in py 3+
#
# print(list(some_list))

# some_list = filter(lambda x: x % 2 == 0, some_list)  # return generator in py 3+
#
# print(list(some_list))

value = reduce(lambda x, y: x + y, some_list)
print(value)


