class Car:
    def __init__(self, body_color, drive_type, horsepower):
        self.body_color = body_color
        self.drive_type = drive_type
        self.horsepower = horsepower

    def ride(self, driveway="everywhere"):
        print(f"I can drive {driveway}!")

    def parking(self):
        print("I can Park in the Parking lot!")

    def age(self, age_year=0):
        print(f"My age is {age_year}")


class Airplane:
    def __init__(self, body_color, engine_type, passengers_number):
        self.body_color = body_color
        self.engine_type = engine_type
        self.passengers_number = passengers_number

    def fly(self):
        print("I believe I can fly...")

    def refuel(self, fuel="kerosene"):
        print(f"I fill up with {fuel}")

    def carry_passengers(self, num_passengers=0):
        if num_passengers >= self.passengers_number:
            print("I'm filled with passengers")
        elif 0 < num_passengers < self.passengers_number:
            print(f"I carry {num_passengers} passengers")
        else:
            print("I'm empty!")


class Ship:
    def __init__(self, body_color, engine_type, ship_displacement):
        self.body_color = body_color
        self.engine_type = engine_type
        self.ship_displacement = ship_displacement

    def swim(self, water_area="water"):
        print(f"I can swim on {water_area}")

    def parking(self):
        print("I can Park in the Harbor!")

    def fuel_filling(self, fuel="electricity"):
        print(f"I swim on {fuel}")


class AmphibiousCar(Car, Ship):
    def __init__(self, body_color, drive_type, horsepower, engine_type, ship_displacement):
        Car.__init__(self, body_color, drive_type, horsepower)
        Ship.__init__(self, body_color, engine_type, ship_displacement)

    def parking(self):
        print("I can Park in the Harbor and in the Parking lot!")
