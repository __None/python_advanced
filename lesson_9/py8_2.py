import time
from functools import reduce

def some_method(x):
    if x % 2 == 0:
        return x
    return x + 1


if __name__ == '__main__':
    some_list = [1, 2, 3, 4, 5]

    # some_list = [x + 1 for x in some_list]

    # some_list = map(some_method, some_list)
    #
    # print(list(some_list))

    # some_list = filter(lambda x: x % 2 == 0, some_list)
    #
    # def even(x):
    #     return x % 2 == 0
    #
    # some_list = filter(even, some_list)
    #
    # print(list(some_list))

    # value = reduce(lambda x, y: x + y, some_list)
    # print(value)
    # print(sum(some_list))


class Cat:
    name = None
    color = None

    def __init__(self, name, color=None):
        self.name = name
        self.color = color or 'Black'
        # if color:
        #     self.color = color

    def __del__(self):
        print(f'Cat {self.name} dead now')

    def __str__(self):
        return f"{self.name} {self.color}"

    # def __unicode__(self):

    # def __int__(self):
    # def __float__(self):
    # def __len__(self):

    def _protected(self):
        pass

    def __private(self):
        pass

    def walk(self):
        print(f"{self.name} is walking now")


class MegaCat(Cat):
    age = 22

    def __init__(self, *args, **kwargs):
        self.age = kwargs.pop('age', 0)
        super().__init__(*args, **kwargs)

    def walk(self):
        super().walk()
        print(f'{self.name} walk as cool cat!')


class Bird:
    name = 'Kesha'

    def fly(self):
        print(f"{self.name} is flight now!")

    def walk(self):
        print(f"{self.name} walk as a bird now!")


class SwimMixin:

    def swim(self):
        print(f'{self.name} swimming now!')


class CatBird(Bird, SwimMixin, Cat):

    def walk(self):
        print(f'{self.name} walk in wierd way!')

    def __del__(self):
        pass


catbird = CatBird(name='Fenrir', color='Red')

catbird.walk()
catbird.fly()
catbird.swim()
cat1 = Cat(name='Tor', color='Black')
# del cat1
# time.sleep(10)
# cat1._protected()
# cat1._Cat__private()
# print(dir(cat1))

cat2 = Cat(name='Izum', color='Ginger')
cat3 = MegaCat(name='Maguns', color='White', age=34)

# cat1.walk()
# cat3.walk()

# print(cat3.age)

# cat1 = Cat()
# cat2 = Cat()

# cat1.legs_count = 3

# cat1_str = str(cat1)
# print(cat1_str)
# print(cat1, cat2)
# print(cat1.legs_count)
# cat1.walk()
# print(type(cat1))
