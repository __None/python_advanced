from re import findall, sub


def get_file_contents(file_name):
    with open(file_name, "r") as opened_file:
        file_contents = opened_file.read()
        return file_contents


# Task 1


def task1(file_name):
    file_contents = get_file_contents(file_name)
    date_list = findall(r"[0-3]*\d-[0-1]*\d-\d{4}", file_contents)
    print(date_list)


# task1(something.txt)

# Task 2

# def leap_year_julian():
#     while True:
#         try:
#             user_year = int(input("Enter long of list: "))
#             if -45 <= int(user_year) < 1582:  # the negative number is the year BC
#                 return int(user_year)
#             print("Incorrect input, try again!")
#         except ValueError:
#             print("Incorrect input, try again!")


def leap_year_gregorian():
    while True:
        user_year = input("Enter long of list: ")
        if user_year.isdigit():
            if 1582 <= int(user_year):
                return int(user_year)
            print("Incorrect input, try again!")
        else:
            print("Incorrect input, try again!")


def leap_or_no():
    year_from_user = leap_year_gregorian()
    # year_from_user = leap_year_julian()
    if year_from_user % 4 != 0:
        print(f"{year_from_user} is not a leap year!")
    elif year_from_user % 100 == 0:
        if year_from_user % 400 == 0:
            print(f"{year_from_user} is a leap year!")
        else:
            print(f"{year_from_user} is not a leap year!")
    else:
        print(f"{year_from_user} is a leap year!")


# leap_or_no()

# Task 3


def func_task3(file_name):
    file_contents = get_file_contents(file_name)
    number_list = findall(r"\D(-?\d+\.?\d*)", file_contents)
    sum_task3 = 0
    for value in number_list:
        sum_task3 += float(value)
    print(f"Sum of all numbers from text is {sum_task3}!")


# func_task3("something.txt")

# Task 4


def geometric_progression(iter_number, start_member=1, denominator=2):
    for index in range(iter_number):
        start_member *= denominator
        yield start_member


# Task 5


def func_task5(file_path):
    with open(file_path, "r+") as file_task5:
        file_contents = file_task5.read()
        text_without_negative = sub(r"\D(-\d+\.?\d*)", "", file_contents)
        new_file_contents = text_without_negative.ljust(len(file_contents))
        file_task5.seek(0)
        file_task5.write(new_file_contents)


# func_task5("something.txt")

# Task 6 It is not specified that there should be only names and geographical names :)


def func_task6(file_name):
    file_contents = get_file_contents(file_name)
    answer_list = findall(r"\b[A-Z][a-z]+\b", file_contents)
    print(answer_list)


# func_task6("something.txt")
