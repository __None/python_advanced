class Price:
    value = 0

    def __get__(self, instance, owner):
        if owner.discount:
            self.value -= (self.value / 100 * instance.value)
        return self.value

    def __set__(self, instance, value):
        self.value = value


class Product:
    # price = 100
    discount = 20
    price = Price()

    def __init__(self, price=None):
        self.price = price if price else self.price


# price = 200
# discount = 20
# if discount:
#     price -= (price * 100 / discount)
# product = Product(price=price)
# product = Product(price=200)
# product.discount = 20
print(product.price)

