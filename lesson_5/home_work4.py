latin_alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                  "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]  # 26

while True:
    user_input = input("Enter your shift (int, plz): ")
    if user_input.isdigit():
        user_shift = int(user_input)
        with open("original.txt", "r") as original_file:
            open_message = original_file.read()
            with open("encrypted.txt", "w") as encrypted_file:
                for char in open_message:
                    if char.isalpha():
                        if char.isupper():
                            up_to_low = char.lower()
                            new_index = (latin_alphabet.index(up_to_low) + user_shift) % 26
                            encrypted_file.write(latin_alphabet[new_index].upper())
                            # encrypted_file.write(latin_alphabet[(latin_alphabet.index(char.lower()) + user_shift) % 26].upper())
                        else:
                            new_index = (latin_alphabet.index(char) + user_shift) % 26
                            # encrypted_file.write(latin_alphabet[(latin_alphabet.index(char) + user_shift) % 26])
                            encrypted_file.write(latin_alphabet[new_index])
                    elif char.isdigit():
                        encrypted_file.write(str((int(char) + user_shift) % 9))
                    else:
                        encrypted_file.write(char)

        break
