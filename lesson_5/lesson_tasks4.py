# Tasks from the fifth lecture (slide number 7)
# Task 1
first_name_task1 = input("Enter your first name: ")
last_name_task1 = input("Enter your last name: ")
with open("task1.txt", "a") as file_task1:
    file_task1.writelines([first_name_task1, last_name_task1])

# Task 2
with open("task2.txt", "r") as file_task2:
    str_list_task2 = file_task2.readlines()
    sum_list = []
    for file_str in str_list_task2:
        file_str.replace("\n", "")
        char_list_task2 = file_str.split(" ")
        temp_sum = 0
        for char in char_list_task2:
            temp_sum += int(char)
        sum_list.append(temp_sum)
    for index in range(len(sum_list)):
        print(f"The sum of the {index} line is {sum_list[index]}")

# Task 3
first_number_task3 = int(input("Enter first value: "))
second_number_task3 = int(input("Enter second value: "))

with open("task3.txt", "w") as file_task3:
    file_task3.write(str(first_number_task3 - second_number_task3))
