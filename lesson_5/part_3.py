import io
try:  # It is slowly then "if"
    with open("some.txt", "w") as my_file:
        my_file.write("TEXT")

    # test_var = 2/0
    value = "2w"
    if not value.isdigit():
        raise ZeroDivisionError  # create your exception
except io.UnsupportedOperation:
    print("Your write but your must read!")
else:
    print("It's fiasco")
finally:
    print("I've done")

# except:  # it's wrong
# except Exception as ex:
#     print(ex)
