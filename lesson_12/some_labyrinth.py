labyrinth = ["##########",
             ".........#",
             "######.###",
             "#......###",
             "#.####.###",
             "#........#",
             "##.#######",
             "##.##.####",
             "##......##",
             "#######.##"]

enter_labyrinth = [1, 0]


def road_check(next_pose):
    current_pose = next_pose.copy()
    possible_moves = []
    for column in range(current_pose[1] - 1, current_pose[1] + 2):
        for row in range(current_pose[0] - 1, current_pose[0] + 2):
            if column < 0 or column > len(labyrinth) - 1:
                continue
            elif row < 0 or row > len(labyrinth[column]) - 1:
                continue
            else:
                if labyrinth[row][column] == "." and (row == current_pose[0] or column == current_pose[1]):
                    if not possible_moves.count([row, column]):
                        possible_moves.append([row, column])
    return possible_moves


def where_exit(made_moves):
    copy_moves = made_moves.copy()
    copy_moves.remove(enter_labyrinth)
    for index in range(0, len(labyrinth) - 1):
        if copy_moves.count([0, index]):
            print(f"Exit is {[0, index]}")
        if copy_moves.count([len(labyrinth) - 1, index]):
            print(f"Exit is {[len(labyrinth) - 1, index]}")
        if copy_moves.count([index, 0]):
            print(f"Exit is {[index, 0]}")
        if copy_moves.count([index, len(labyrinth) - 1]):
            print(f"Exit is {[index, len(labyrinth) - 1]}")


def traversed_path():
    made_moves = [enter_labyrinth]
    go_to = iter(made_moves)
    for go_to in go_to:
        new_moves = road_check(go_to)
        for some_move in new_moves:
            if not made_moves.count(some_move):
                made_moves.append(some_move)
    where_exit(made_moves)


# traversed_path()
