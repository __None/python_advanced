from random import randrange


def add(x, y, c=None):
    if c is not None:
        try:
            return x + y / c
        except:
            return get_me_some_random()
    return x + y


def get_me_some_random():
    value = randrange(0, 10)
    if value > 5:
        return 'Cool'
    return 'Not Cool'


class Cat:
    name = None
    color = None

    def __init__(self, name, color=None):
        self.name = name
        self.color = color or 'Black'

    def __del__(self):
        print(f'Cat {self.name} dead now')

    def __str__(self):
        return f"{self.name} {self.color}"

    def walk(self):
        return f"{self.name} is walking now"


def hug(cat):
    if cat.color == 'Black':
        return cat.walk()
    else:
        return cat.color
