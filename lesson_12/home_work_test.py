from unittest import TestCase
from unittest.mock import patch
from io import StringIO


import some_labyrinth


class LabyrinthTests(TestCase):

    def test_road_check_if(self):
        some_labyrinth.enter_labyrinth = [1, 0]
        some_labyrinth.labyrinth = ["##########",
                               ".........#",
                               "######.###",
                               "#......###",
                               "#.####.###",
                               "#.........",
                               "##.#######",
                               "##.##.####",
                               "##......##",
                               "##########"]
        expected_value = [[5, 8], [5, 9]]
        value = some_labyrinth.road_check([5, 9])
        self.assertEqual(expected_value, value)

    def test_road_check_elif(self):
        some_labyrinth.enter_labyrinth = [1, 0]
        some_labyrinth.labyrinth = ["##########",
                               ".........#",
                               "######.###",
                               "#......###",
                               "#.####.###",
                               "#........#",
                               "##.#######",
                               "##.##.####",
                               "##......##",
                               "#######.##"]
        expected_value = [[8, 7], [9, 7]]
        value = some_labyrinth.road_check([9, 7])
        self.assertEqual(expected_value, value)

    def test_road_check_else(self):
        some_labyrinth.enter_labyrinth = [1, 0]
        some_labyrinth.labyrinth = ["##########",
                               ".........#",
                               "######.###",
                               "#......###",
                               "#.####.###",
                               "#........#",
                               "##.#######",
                               "##.##.####",
                               "##......##",
                               "#######.##"]
        expected_value = [[3, 4], [3, 5], [3, 6]]
        value = some_labyrinth.road_check([3, 5])
        self.assertEqual(expected_value, value)

    @patch('sys.stdout', new_callable=StringIO)
    def test_where_exit_up(self, mock_stdout):
        some_labyrinth.enter_labyrinth = [1, 0]
        some_labyrinth.labyrinth = ["#######.##",
                               ".........#",
                               "######.###",
                               "#......###",
                               "#.####.###",
                               "#........#",
                               "##.#######",
                               "##.##.####",
                               "##......##",
                               "##########"]
        expected_value = "Exit is [0, 7]\n"
        some_labyrinth.where_exit([[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [2, 6], [1, 7], [3, 6], [0, 7],
                                   [1, 8], [3, 5], [4, 6], [3, 4], [5, 6], [3, 3], [5, 5], [5, 7], [3, 2], [5, 4], [5, 8],
                                   [3, 1], [5, 3], [4, 1], [5, 2], [5, 1], [6, 2], [7, 2], [8, 2], [8, 3], [8, 4], [8, 5],
                                   [7, 5], [8, 6], [8, 7]])
        self.assertEqual(expected_value, mock_stdout.getvalue())

    @patch('sys.stdout', new_callable=StringIO)
    def test_where_exit_down(self, mock_stdout):
        some_labyrinth.enter_labyrinth = [1, 0]
        some_labyrinth.labyrinth = ["##########",
                               ".........#",
                               "######.###",
                               "#......###",
                               "#.####.###",
                               "#........#",
                               "##.#######",
                               "##.##.####",
                               "##......##",
                               "#######.##"]
        expected_value = "Exit is [9, 7]\n"
        some_labyrinth.where_exit([[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [2, 6], [1, 7], [3, 6], [1, 8],
                                   [3, 5], [4, 6], [3, 4], [5, 6], [3, 3], [5, 5], [5, 7], [3, 2], [5, 4], [5, 8], [3, 1],
                                   [5, 3], [4, 1], [5, 2], [5, 1], [6, 2], [7, 2], [8, 2], [8, 3], [8, 4], [8, 5], [7, 5],
                                   [8, 6], [8, 7], [9, 7]])
        self.assertEqual(expected_value, mock_stdout.getvalue())

    @patch('sys.stdout', new_callable=StringIO)
    def test_where_exit_left(self, mock_stdout):
        some_labyrinth.enter_labyrinth = [1, 0]
        some_labyrinth.labyrinth = ["##########",
                               ".........#",
                               "######.###",
                               "#......###",
                               "#.####.###",
                               ".........#",
                               "##.#######",
                               "##.##.####",
                               "##......##",
                               "##########"]
        expected_value = "Exit is [5, 0]\n"
        some_labyrinth.where_exit([[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [2, 6], [1, 7], [3, 6], [1, 8],
                                   [3, 5], [4, 6], [3, 4], [5, 6], [3, 3], [5, 5], [5, 7], [3, 2], [5, 4], [5, 8], [3, 1],
                                   [5, 3], [4, 1], [5, 2], [5, 1], [6, 2], [5, 0], [7, 2], [8, 2], [8, 3], [8, 4], [8, 5],
                                   [7, 5], [8, 6], [8, 7]])
        self.assertEqual(expected_value, mock_stdout.getvalue())

    @patch('sys.stdout', new_callable=StringIO)
    def test_where_exit_right(self, mock_stdout):
        some_labyrinth.enter_labyrinth = [1, 0]
        some_labyrinth.labyrinth = ["##########",
                               ".........#",
                               "######.###",
                               "#......###",
                               "#.####.###",
                               "#.........",
                               "##.#######",
                               "##.##.####",
                               "##......##",
                               "##########"]
        expected_value = "Exit is [5, 9]\n"
        some_labyrinth.where_exit([[1, 0], [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [2, 6], [1, 7], [3, 6], [1, 8],
                                   [3, 5], [4, 6], [3, 4], [5, 6], [3, 3], [5, 5], [5, 7], [3, 2], [5, 4], [5, 8], [3, 1],
                                   [5, 3], [5, 9], [4, 1], [5, 2], [5, 1], [6, 2], [7, 2], [8, 2], [8, 3], [8, 4], [8, 5],
                                   [7, 5], [8, 6], [8, 7]])
        self.assertEqual(expected_value, mock_stdout.getvalue())

    @patch("some_labyrinth.road_check", return_value=[[1, 0], [1, 1]])
    @patch("some_labyrinth.where_exit", return_value=None)
    def test_traversed_path(self, mock_exit, mock_road_check):
        some_labyrinth.enter_labyrinth = [1, 0]
        expected_value = [[1, 0], [1, 1]]
        some_labyrinth.traversed_path()
        self.assertEqual(expected_value, mock_exit.call_args[0][0])
