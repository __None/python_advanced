# class Foo:
#     bar = 'qwe'
#
#
# @staticmethod
# def some(x, y):
#     return x + y
import random


class Bird:
    color = 'Brown'
    name = 'Owl'

    def __bool__(self):
        print('BOOL!!!!!')
        return True

    def __len__(self):
        print('LEN!!!!!!')
        return 10


class UpperMetaClass(type):

    def __new__(cls, class_name, class_parents, class_attrs):
        new_attrs = {}
        for name, value in class_attrs.items():
            if not name.startswith('__') and not name.endswith('__'):
                new_attrs[name.upper()] = value
        new_attrs['__bool__'] = lambda self: False
        return super(UpperMetaClass, cls).__new__(cls, class_name,
                                                  class_parents,
                                                  new_attrs)


def my_func_metaclass(class_name, class_parents, class_attrs):
    class_attrs['version'] = '1.0'
    class_parents += (Bird, )
    return type(class_name, class_parents, class_attrs)


class Foo(metaclass=my_func_metaclass):
    bar = 12
    # _metaclass_ = UpperMetaClass

    def some(self):
        print("I'm Alive!")

    # def __getattribute__(self, item):
    #     print(f'You try to get an {item}')
    #     return item


def foo(a=[]):
    a.append(1)


print(foo.__defaults__)
foo()
print(foo.__defaults__)
foo()
print(foo.__defaults__)

# class Payment:
#
#     status = 'starting'
#     __allowed = {
#         'starting': ['charge', 'commit']
#     }
#
#     def charge(self):
#         print('Charge')
#
#     def commit(self):
#         print('Commit')

    # def __getattribute__(self, item):
    #     if item not in ['_Payment__allowed'] and item in self.__allowed[self.status]:
    #         return item
    #     raise KeyError('Not allowed for this state')


# payment = Payment()
# payment.charge()


# print(Foo().BAR)
# Foo().SOME()
# print(Foo().bar)


class Cat:
    name = None
    color = None
    weight = None

    def __init__(self, name, color=None, weight=5):
        self.name = name
        self.color = color or 'Black'
        self.weight = weight

    def __del__(self):
        print(f'Cat {self.name} dead now')

    def __str__(self):
        return f"{self.name} {self.color}"

    def __add__(self, other):
        # if isinstance(other, Cat):
        return Cat(
            name=f'{self.name}{other.name}',
            color=random.choice([self.color, other.color])
        )
        # raise TypeError('Can add Cat instance only')

    def __radd__(self, other):
        return self.__add__(other)

    def __lt__(self, other):
        return self.weight < other.weight

    def __eq__(self, other):
        return self.weight == other.weight

    def walk(self):
        print(f"{self.name} is walking now")


if __name__ == '__main__':
    tor = Cat(name='Tor')
    brunhilda = Cat(name='Brunhilda', color='White')
    bird = Bird()
    kitty = bird + tor

    # del bird.__bool__

    if bird:
        print("HERE!")
        pass

    # if tor == brunhilda:
    #     kitty = tor + brunhilda
    # else:
    #     kitty = tor + bird

    # print(kitty)
    # print(Foo.__class__)
    # print(type(Foo()))
    # Bar = type('Bar', (Foo, ), {
    #     'foo': 123,
    #     'some': some
    # })
    # print(type(Bar()))
    # bar = Bar()
    # print(bar.foo, bar.bar, bar.some(1, 1))
