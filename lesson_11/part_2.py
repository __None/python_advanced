# object


class UpperMetaClass(type):

    def __new__(mcs, class_name, class_parents, class_attrs):
        new_attrs = {}
        for name, value in class_attrs.items():
            if not name.startswich("__") and not name.endswich("__"):
                new_attrs[name.upper()] = value
        return super(UpperMetaClass, mcs).__new__(mcs, class_name, class_parents, new_attrs)


class Foo:
    bar = 12

    def some(self):
        print("I'm Alive!")


# print(Foo.bar)
print(Foo.BAR)

