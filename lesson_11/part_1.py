class Foo:
    bar = "qwe"


def some(self, x, y):
    return x + y

# print(Foo.__class__)
# print(type(Foo))


Bar = type("Bar", (Foo, ), {"foo": 123, "some": some})
# print(type(Bar()))
bar = Bar()
print(bar.foo, bar.bar, bar.some(1, 1))


