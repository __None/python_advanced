from os import path
from datetime import date, timedelta
from json import dump, load
from re import findall
from functools import reduce


class AppleShop:

    def __init__(self, my_money=0):
        self.today = date.today()
        self.products = [{"title": "MacBook", "sale_price": 10000, "receipt_price": 5000, "storage": 0},
                    {"title": "iPhone", "sale_price": 2000, "receipt_price": 1000, "storage": 0},
                    {"title": "iPod", "sale_price": 300, "receipt_price": 150, "storage": 0}, ]
        self.my_money = my_money
        self.__spent_money = 0
        self.__earned_money = 0

    def __enter__(self):
        if path.isfile("temp_data.json"):
            with open("temp_data.json", "r") as json_file:
                list_from_json = load(json_file)
                self.products = list_from_json[0]
                self.my_money = list_from_json[1]
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not path.isfile("logfile.txt"):
            with open("logfile.txt", "w"):
                pass
        with open("logfile.txt", "a") as logfile:
            logfile.writelines(f"""{self.today} money={self.my_money} spent={self.__spent_money
            } earned={self.__earned_money}\n""")
            self.__spent_money = 0
            self.__earned_money = 0

        with open("temp_data.json", "w") as json_file:
            list_for_json = [self.products, self.my_money]
            dump(list_for_json, json_file, indent=4)

    def receipt(self, product, number):
        for items in self.products:
            if items["title"] == product:
                if items["receipt_price"] * number > self.my_money:
                    print("Not enough money!")
                else:
                    self.__spent_money += items["receipt_price"] * number
                    self.my_money -= items["receipt_price"] * number
                    items["storage"] += number

    def sale(self, product, number):
        for items in self.products:
            if items["title"] == product:
                if items["storage"] < number:
                    print("There are not enough product!")
                    number = items["storage"]
                self.__earned_money += items["sale_price"] * number
                self.my_money += items["sale_price"] * number
                items["storage"] -= number

    def __logs_read(self):
        if not path.isfile("logfile.txt"):
            print("No data yet!")
        else:
            with open("logfile.txt", "r") as logfile:
                log_data = logfile.read()
                if not log_data:
                    # print("No data yet!")
                    return 0
                else:
                    return log_data

    def __correct_date(self, input_date):
        year = findall(r"\d{4}", input_date)
        month = findall(r"\D([0-1]*\d)\D", input_date)
        day = findall(r"\D([0-3]*\d)$", input_date)
        return int(year[0]), int(month[0]), int(day[0])

    def __daily_report(self, desired_day, logs):
        spend = 0
        earned = 0
        day_events = findall(str(desired_day) + r"(.*)\n", logs)
        if day_events:
            day_events_iter = iter(day_events)
            for events in day_events_iter:
                spend_list = [int(x) for x in findall(r"spent=(\d+)", events)]
                earned_list = [int(x) for x in findall(r"earned=(-?\d+)", events)]
                spend += reduce(lambda x, y: x + y, spend_list)
                earned += reduce(lambda x, y: x + y, earned_list)
        return earned, spend

    def show_daily_report(self, want_date):
        try:
            year, month, day = self.__correct_date(want_date)
            log_data = self.__logs_read()
            if log_data:
                earned, spend = self.__daily_report(desired_day=date(year, month, day), logs=log_data)
                print(f"Daily report: earned - {earned}, spent - {spend}, profit - {earned - spend}")
            else:
                print("No data yet!")
        except ValueError:
            print("There is no such date!")

    def week_report(self, want_date):
        try:
            log_data = self.__logs_read()
            if log_data:
                year, month, day = self.__correct_date(want_date)
                correct_want_date = date(year, month, day)
                week_ago = date(year, month, day) - timedelta(days=7)
                earned = 0
                spend = 0
                while correct_want_date != week_ago:
                    earned_per_day, spend_per_day = self.__daily_report(desired_day=correct_want_date, logs=log_data)
                    earned += earned_per_day
                    spend += spend_per_day
                    correct_want_date -= timedelta(days=1)
                print(f"Report for the week earned - {earned}, spent - {spend}, profit - {earned - spend}")
            else:
                print("No data yet!")
        except ValueError:
            print("There is no such date!")

    def month_report(self, want_month):
        try:
            log_data = self.__logs_read()
            if log_data:
                year, month, day = self.__correct_date(want_month)
                correct_want_date = date(year, month, day)
                month_ago = date(year, month - 1, day)
                earned = 0
                spend = 0
                while correct_want_date != month_ago:
                    earned_per_day, spend_per_day = self.__daily_report(desired_day=correct_want_date, logs=log_data)
                    earned += earned_per_day
                    spend += spend_per_day
                    correct_want_date -= timedelta(days=1)
                print(f"Report for the month earned - {earned}, spent - {spend}, profit - {earned - spend}")
            else:
                print("No data yet!")
        except ValueError:
            print("There is no such date!")

    def year_report(self, want_year):
        try:
            log_data = self.__logs_read()
            if log_data:
                year, month, day = self.__correct_date(want_year)
                correct_want_date = date(year, month, day)
                year_ago = date(year - 1, month, day)
                earned = 0
                spend = 0
                while correct_want_date != year_ago:
                    earned_per_day, spend_per_day = self.__daily_report(desired_day=correct_want_date, logs=log_data)
                    earned += earned_per_day
                    spend += spend_per_day
                    correct_want_date -= timedelta(days=1)
                print(f"Report for the year earned - {earned}, spent - {spend}, profit - {earned - spend}")
            else:
                print("No data yet!")
        except ValueError:
            print("There is no such date!")


with AppleShop(100000) as shopper:
    receipt_products = (("MacBook", 10), ("iPhone", 10), ("iPod", 10))
    sale_products = (("MacBook", 7), ("iPhone", 5), ("iPod", 10))

    for item, value in receipt_products:
        shopper.receipt(item, value)

    for item, value in sale_products:
        shopper.sale(item, value)

    shopper.show_daily_report("2019-12-04")
    shopper.week_report("2019-12-04")
    shopper.month_report("2019-12-04")
    shopper.year_report("2020-12-01")
    shopper.sale("MacBook", 0)

