import json


some_var = [1, {"name": "Loki"}, "ololo"]
with open("some.json", "w") as json_file:
    json.dump(some_var, json_file)

with open("some.json", "r") as json_file:
    data = json.load(json_file)
    # print(data)

print(json.dumps(some_var))
