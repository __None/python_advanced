# Task 1
from time import perf_counter
from time import sleep


def execution_time(func_to_wrap):

    def wrapper(*args, **kwargs):
        start_time = perf_counter()
        func_to_wrap(*args, **kwargs)
        print(f"The function lasted for {perf_counter() - start_time} sec.")
        return func_to_wrap

    return wrapper


@execution_time
def some_func():
    print("I'm start!")
    sleep(10)
    print("I'm end!")


# some_func()


# Task 2

def correct_input():
    while True:
        long = input("Enter long of list: ")
        if long.isdigit():
            return int(long)
        else:
            print("Incorrect input, try again!")


def something_func():
    long = correct_input()
    number_list = []
    number_var = 1
    while len(number_list) < long:
        if number_var % 3 == 0:
            number_list.append(number_var)
        number_var += 1
    number_list.reverse()
    return number_list


# print(something_func())

# Task 3


def skip(return_parameter=None):

    def inner(func_to_wrap):

        def wrapper(*args, **kwargs):
            if return_parameter is not None:
                return return_parameter
            data = func_to_wrap(*args, **kwargs)
            return data

        return wrapper

    return inner


# Task 4


def triangular_number(iter_number=0, start_member=0):
    for index in range(start_member, iter_number):
        func_answer = 1/2 * index * (index + 1)
        yield int(func_answer)

