import pickle
import json
import time
import os

if __name__ == '__main__':
    some_var = [1, {'name': 'Loki'}, 'ololo']

    # with open('some.dump', 'wb') as dump_file:
    #     pickle.dump(some_var, dump_file)
    #
    # with open('some.dump', 'rb') as dump_file:
    #     my_data = pickle.load(dump_file)
    #     print(my_data)
    #
    # print(pickle.dumps(some_var))

    # with open('some.json', 'w') as json_file:
    #     json.dump(some_var, json_file, indent=4)
    #
    # with open('some.json', 'r') as json_file:
    #     data = json.load(json_file)
    #     print(data)
    #
    # print(json.dumps(some_var))
    # print(type(json.loads('{"name": "loki"}')))

    # def cache(suffix=''):
    #
    #     def inner(func_to_wrapp):
    #
    #         def wrapper(*args, **kwargs):
    #             cache_name = f'{"_".join([str(x) for x in args])}{suffix}.cache'
    #             if os.path.exists(cache_name):
    #                 with open(cache_name, 'rb') as cache:
    #                     return pickle.load(cache)
    #
    #             data = func_to_wrapp(*args, **kwargs)
    #
    #             with open(cache_name, 'wb') as cache:
    #                 pickle.dump(data, cache)
    #
    #             return data
    #
    #         return wrapper
    #
    #     return inner
    #
    # @cache()
    # def some_long_time_method(x, y):
    #     time.sleep(10)
    #     return x + y
    #
    # # @cache(suffix='div')
    # def some_long_time_method1(x, y):
    #     time.sleep(10)
    #     return x - y
    #
    # @cache()
    # def some_long_time_method2(x, y, t):
    #     time.sleep(10)
    #     return x - y + t
    #
    # print(some_long_time_method1(2, 2))
    # some_long_time_method2(2, 2, 2)

    import threading
    import multiprocessing

    def my_thread(timer):
        time.sleep(timer)
        print('Thread done')

    def main():
        counter = 0
        while counter <= 40:
            print(counter)
            counter += 1
            time.sleep(1)

    process = multiprocessing.Process(
        target=my_thread,
        # args=(20, ),
        kwargs={'timer': 20}
    )
    # thread = threading.Thread(
    #     target=my_thread,
    #     args=(20, ),
    #     name='my_thread'
    #     # kwargs={'timer': 20}
    # )
    # thread.start()
    process.start()

    # main()
    # process.join()
    # time.sleep(20)
