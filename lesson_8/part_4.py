import threading
import multiprocessing
import time


def my_func_threading(timer):
    time.sleep(timer)
    print("Threading done")


def main():
    counter = 0
    while counter <= 40:
        print(counter)
        counter += 1
        time.sleep(1)


# thread = threading.Thread(target=my_func_threading, args=(20, ), name="my_thread")
# thread.start()
process = multiprocessing.Process(target=my_func_threading, args=(20, ))
process.start()
main()
process.join()

