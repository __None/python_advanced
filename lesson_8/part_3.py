import os
import pickle
import time


# def some_long_time_method(x, y):
#     if os.path.exists(f"{x}_{y}sum.cache"):
#         with open(f"{x}_{y}sum.cache", "rb") as cache:
#             return pickle.loads(cache)
#     time.sleep(10)
#     with open(f"{x}_{y}sum.cache", "wb") as cache:
#         pickle.dump(x + y, cache)
#     return x + y


def cache(func_to_wrapp):
    print("Inside a wrapper")

    def wrapper(*args, **kwargs):
        print("Before function")
        data = func_to_wrapp(*args, **kwargs)
        print("After function")
        return data

    return wrapper


@cache
def some_long_time_method1(x, y):
    time.sleep(10)
    return x - y


@cache
def some_long_time_method2(x, y):
    time.sleep(10)
    return x * y


print(some_long_time_method2(2, 2))

