import pickle


some_var = [1, {"name": "Loki"}, "ololo"]
with open("some.dump", "wb") as dump_file:
    pickle.dump(some_var, dump_file)

with open("some.dump", "rb") as dump_file:
    my_data = pickle.load(dump_file)
    # print(my_data)

# print(pickle.dumps(some_var))


