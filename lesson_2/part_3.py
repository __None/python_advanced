print(int("1010101", 2))  # second type!
print(int("FACCB", 16))

first_str = "test"
second_str = "str"
print(first_str + second_str)
print("*" * 40)
print(first_str * 10)

print(bin(4567))

eval("print('HA HA HA!')")

print(globals())

print(int(round(3.54)))
