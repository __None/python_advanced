# Task 1
user_name = input("Enter your name: ")
user_age = input("Enter your age: ")

print("Hello", user_name, "your age is", user_age)
# print("Hello %s your age is %s" % (user_name, user_age))  # Can I do this?

# Task 2
value_task2 = int(input("Enter your value: "))
print("Residue from division on 3 number of", value_task2, "in 132 extend is", pow(value_task2, 132, 3))

# Task 3
a_value_task3 = int(input("Enter 'a' value: "))
b_value_task3 = int(input("Enter 'b' value: "))

print("The result of a + b is:", a_value_task3 + b_value_task3)
print("The result of a - b is:", a_value_task3 - b_value_task3)
print("The result of a * b is:", a_value_task3 * b_value_task3)
print("The result of a / b is:", a_value_task3 / b_value_task3)

# Task 4
print("Your formula is: 2a-8b/(a-b+c)")

a_value_task4 = int(input("Enter 'a' value: "))
b_value_task4 = int(input("Enter 'b' value: "))
c_value_task4 = int(input("Enter 'c' value: "))

print("Result is:", 2 * a_value_task4 - 8 * b_value_task4 / (a_value_task4 - b_value_task4 + c_value_task4))

# Task 5
string_task5 = input("Enter your string: ")
value_task5 = int(input("Enter you value: "))
print(string_task5 * value_task5)

# Task 6
first_value_task6 = 125
second_value_task6 = 437

print("The remainder of the division of 125 by 2 is:", first_value_task6 % 2)
print("The remainder of the division of 125 by 3 is:", first_value_task6 % 3)
print("The remainder of the division of 125 by 10 is:", first_value_task6 % 10)
print("The remainder of the division of 125 by 22 is:", first_value_task6 % 22)

print("The remainder of the division of 437 by 2 is:", second_value_task6 % 2)
print("The remainder of the division of 437 by 3 is:", second_value_task6 % 3)
print("The remainder of the division of 437 by 10 is:", second_value_task6 % 10)
print("The remainder of the division of 437 by 22 is:", second_value_task6 % 22)

# Task 7
first_value_task7 = int(input("Enter your value 'a': "))
second_value_task7 = int(input("Enter your value 'b': "))

print("Integer part a / b is:", first_value_task7 // second_value_task7)
print("Integer part b / a is:", second_value_task7 // first_value_task7)

# Task 8
string1_task8 = input("Enter first string: ")
string2_task8 = input("Enter second string: ")
string3_task8 = input("Enter third string: ")

print(string1_task8 + " " + string2_task8 + " " + string3_task8)

# Task 9
first = 15
second = 43

temp_var = first
first = second
second = temp_var
# first, second = second, first  # Can I do this?)

print("First is:", first, "Second is:", second)
