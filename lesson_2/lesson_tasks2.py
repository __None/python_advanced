# Task 1
value_task1 = int(input("Enter value for first task: "))
print(bin(value_task1))

# Task 2
value_task2 = input("Enter value for second task: ")
print(len(value_task2))

# Task 3
value_task3 = float(input("Enter value for third task: "))
print(round(value_task3, 2))

# Task 4
value_task4 = input("Enter value for fourth task: ")
print(value_task4.isdigit())

# Task 5
formula_task5 = input("Enter formula for fifth task: ")
print(eval(formula_task5))

# Task 6
code_task6 = int(input("Enter code for sixth task: "))
print(chr(code_task6))

# Task 7
char_task7 = input("Enter char for seventh task: ")
print(ord(char_task7))

# Task 8
complex_task8 = complex(input("Enter complex number for eighth "))
print(id(complex_task8))
