a = 12  # C++ like code - int a = 12

test_number = 12  # var

PI_NUMBER = 3.14  # const global always

WINDOW_WIDTH = 1024
WINDOW_HEIGHT = 768

# lambda x: x ** x  # char name for var alow

test_complex = 5+5j

test_str = """
    anjdsandjs
    msffndlsn
    asdcscd
    
    can\'t
    
"""  # Многострочные строки

my_super_value = None

test_bool = True  # or False

if __name__ == "__main__":
    my_literal = [x ** 2 for x in range(100)]  # char name for var alow
    user_value = int(input("Enter your value: "))
    print(user_value ** 2)

# имя переменных больше 3-х символов, за исключением общепринятых сокращений

username = input("Enter yor name: ")
print("Hi!", username, sep="_")  # Разделение через нижнее подчеркивание
print(type(username))
