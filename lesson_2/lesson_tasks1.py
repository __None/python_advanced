# Task 1
first_value_task1 = int(input("Enter first value for first task: "))
second_value_task1 = int(input("Enter second value for first task: "))

print(first_value_task1 + second_value_task1)

# Task 2
value_task2 = int(input("Enter value for second task: "))
print(value_task2 ** 5)

# Task 3
first_value_task3 = int(input("Enter first value for third task: "))
second_value_task3 = int(input("Enter second value for third task: "))

print(first_value_task3 / second_value_task3)

# Task 4
first_value_task4 = int(input("Enter first value for fourth task: "))
second_value_task4 = int(input("Enter second value for fourth task: "))

print("Sum is:", first_value_task4 + second_value_task4)
print("Dif is:", first_value_task4 - second_value_task4)

# Task 5
first_string_task5 = input("Enter first string for fifth task: ")
second_string_task5 = input("Enter second string for fifth task: ")

print(first_string_task5 * 10)
print(first_string_task5 + second_string_task5)

# Task 6
first_value_task6 = int(input("Enter first value for sixth task: "))
second_value_task6 = int(input("Enter second value for sixth task: "))

print(first_value_task6 // second_value_task6)

# Task 7
string_task7 = input("Enter string for seventh task: ")
print("Here is yor string:", string_task7)

# Task 8
print("2a+4b*6c")

first_value_last8 = int(input("Enter a value for eight task: "))
second_value_last8 = int(input("Enter b value for eight task: "))
third_value_last8 = int(input("Enter c value for eight task: "))

print(2 * first_value_last8 + 4 * second_value_last8 * 6 * third_value_last8)
