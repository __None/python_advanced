import tkinter as ttk
from tkinter import filedialog


class MyApplication(ttk.Frame):
    def __init__(self, *args, **kwargs):
        kwargs['height'] = 500
        kwargs['width'] = 500
        super(MyApplication, self).__init__(*args, **kwargs)
        self.create_gui()

    def __load_handler(self):
        filename = filedialog.askopenfilename()
        with open(filename) as loaded_file:
            self.text_box.delete(1.0, ttk.END)
            self.text_box.insert(1.0, loaded_file.read())

    def __save_handler(self):
        filename = filedialog.asksaveasfilename()
        with open(filename, 'w') as save_file:
            save_file.write(self.text_box.get(1.0, ttk.END))

    def __new_file(self):
        self.text_box.delete(1.0, ttk.END)

    def __exit(self):
        quit(1)

    def create_gui(self):
        mainmenu = ttk.Menu(self)
        self.master.config(menu=mainmenu)

        filemenu = ttk.Menu(mainmenu, tearoff=0)
        filemenu.add_command(label="Открыть...", command=self.__load_handler)
        filemenu.add_command(label="Новый", command=self.__new_file)
        filemenu.add_command(label="Сохранить...", command=self.__save_handler)
        filemenu.add_command(label="Выход", command=self.__exit)

        mainmenu.add_cascade(label="Файл", menu=filemenu)

        self.text_box = ttk.Text(self)
        scroll_bar = ttk.Scrollbar(self)

        scroll_bar['command'] = self.text_box.yview
        self.text_box['yscrollcommand'] = scroll_bar.set

        scroll_bar.pack(fill='y', side='right')
        self.text_box.pack(fill='both', side='bottom')
        self.pack(fill='both', side='top')


if __name__ == '__main__':
    root = ttk.Tk()
    app = MyApplication(
        master=root,
    )
    app.mainloop()
