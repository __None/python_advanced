# from tkinter import Frame, Tk, Label
from tkinter import *


class MyCalculator(Frame):
    def __init__(self, *args, **kwargs):
        self.some_var = "zero"

        kwargs["master"].title("My calculator")
        kwargs["master"].iconbitmap("icon.ico")
        kwargs["master"].geometry("320x430")
        kwargs["master"].minsize(320, 430)  # 475
        # kwargs["master"].maxsize(320, 430)

        self.number_entry = Entry(bd=10,
                                  justify="right",
                                  font=("Arial", 24),
                                  relief="flat",
                                  selectbackground="white",
                                  selectforeground="black"
                                  )  # state=DISABLED, disabledbackground ="white"
        self.number_entry.pack(fill="both", side="top")

        self.number_entry.bind("<Key>", "pass")

        answer_label = Label(anchor="e",
                             bg="white",
                             bd=10,
                             font=("Arial", 13),
                             text="667",
                             fg="grey"
                             )
        answer_label.pack(fill="both", side="top")

        super(MyCalculator, self).__init__(*args, **kwargs)

        self.create_gui()

        # self.number_entry.insert(-1, "666")

    def __delete(self):
        pass

    def __division(self):
        pass

    def __multiplication(self):
        pass

    def __subtraction(self):
        pass

    def __summation(self):
        pass

    def __equally(self):
        pass

    def __pressed_number(self, key):
        self.number_entry.insert(0, key)

    @staticmethod
    def __pressed_7(event):
        print("7")

    def __more_actions(self):
        pass

    def create_gui(self):
        self.pack(fill="both")

        standard_frame = Frame()
        standard_frame.pack(side="left")

        button_frame_7_com = Frame(standard_frame, bg="#1de9b6")
        button_frame_7_com.pack(side="left", fill="both")

        button_frame_8_0 = Frame(standard_frame, bg="#1de9b6")
        button_frame_8_0.pack(side="left", fill="both")

        button_frame_9_eq = Frame(standard_frame, bg="#1de9b6")
        button_frame_9_eq.pack(side="left", fill="both")

        button_frame_actions = Frame(standard_frame, bg="#1de9b6")
        button_frame_actions.pack(side="left", fill="both")

        button_frame_more_actions = Frame(standard_frame, bg="black")
        button_frame_more_actions.pack(side="left", fill="both")

        button_root_symbol = Button(button_frame_7_com,
                                    activebackground="#696969",
                                    activeforeground="white",
                                    bg="#434343",
                                    command=lambda: self.__pressed_number("7"),
                                    fg="white",
                                    font=("Arial", 15, "bold"),
                                    relief="flat",
                                    height=2,
                                    width=5,
                                    text="√")
        button_root_symbol.pack(fill="both")
        button_7 = Button(button_frame_7_com,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("7"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="7"
                          )
        button_7.bind("<7>", self.__pressed_7)
        button_7.pack(fill="both")
        button_4 = Button(button_frame_7_com,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("4"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="4"
                          )
        button_4.pack(fill="both")
        button_1 = Button(button_frame_7_com,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("1"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="1"
                          )
        button_1.pack(fill="both")
        button_com = Button(button_frame_7_com,
                            activebackground="#696969",
                            activeforeground="white",
                            bg="#434343",
                            command=lambda: self.__pressed_number(","),
                            fg="white",
                            font=("Arial", 15, "bold"),
                            relief="flat",
                            height=2,
                            width=5,
                            text=","
                            )
        button_com.pack(fill="both")

        button_percent = Button(button_frame_8_0,
                                activebackground="#696969",
                                activeforeground="white",
                                bg="#434343",
                                command=lambda: self.__pressed_number("8"),
                                fg="white",
                                font=("Arial", 15, "bold"),
                                relief="flat",
                                height=2,
                                width=5,
                                text="%")
        button_percent.pack(fill="both")
        button_8 = Button(button_frame_8_0,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("8"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="8"
                          )
        button_8.pack(fill="both")
        button_5 = Button(button_frame_8_0,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("5"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="5"
                          )
        button_5.pack(fill="both")
        button_2 = Button(button_frame_8_0,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("2"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="2"
                          )
        button_2.pack(fill="both")
        button_zero = Button(button_frame_8_0,
                             activebackground="#696969",
                             activeforeground="white",
                             bg="#434343",
                             command=lambda: self.__pressed_number("0"),
                             fg="white",
                             font=("Arial", 15, "bold"),
                             relief="flat",
                             height=2,
                             width=5,
                             text="0"
                             )
        button_zero.pack(fill="both")

        button_clear = Button(button_frame_9_eq,
                              activebackground="#696969",
                              activeforeground="white",
                              bg="#434343",
                              command=lambda: self.__pressed_number("9"),
                              fg="white",
                              font=("Arial", 15, "bold"),
                              relief="flat",
                              height=2,
                              width=5,
                              text="C")
        button_clear.pack(fill="both")
        button_9 = Button(button_frame_9_eq,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("9"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="9"
                          )
        button_9.pack(fill="both")
        button_6 = Button(button_frame_9_eq,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("6"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="6"
                          )
        button_6.pack(fill="both")
        button_3 = Button(button_frame_9_eq,
                          activebackground="#696969",
                          activeforeground="white",
                          bg="#434343",
                          command=lambda: self.__pressed_number("3"),
                          fg="white",
                          font=("Arial", 15, "bold"),
                          relief="flat",
                          height=2,
                          width=5,
                          text="3"
                          )
        button_3.pack(fill="both")
        button_eq = Button(button_frame_9_eq,
                           activebackground="#696969",
                           activeforeground="white",
                           bg="#434343",
                           command=self.__equally(),
                           fg="white",
                           font=("Arial", 15, "bold"),
                           relief="flat",
                           height=2,
                           width=5,
                           text="="
                           )
        button_eq.pack(fill="both")

        button_del = Button(button_frame_actions,
                            activebackground="#696969",
                            activeforeground="white",
                            bg="#636363",
                            command=self.__delete(),
                            fg="white",
                            font=("Arial", 15, "bold"),
                            relief="flat",
                            height=2,
                            width=5,
                            text="DEL"
                            )
        button_del.pack(fill="both")
        button_div = Button(button_frame_actions,
                            activebackground="#696969",
                            activeforeground="white",
                            bg="#636363",
                            command=self.__division(),
                            fg="white",
                            font=("Arial", 15, "bold"),
                            relief="flat",
                            height=2,
                            width=5,
                            text="÷"
                            )
        button_div.pack(fill="both")
        button_mul = Button(button_frame_actions,
                            activebackground="#696969",
                            activeforeground="white",
                            bg="#636363",
                            command=self.__multiplication(),
                            fg="white",
                            font=("Arial", 15, "bold"),
                            relief="flat",
                            height=2,
                            width=5,
                            text="×"
                            )
        button_mul.pack(fill="both")
        button_sub = Button(button_frame_actions,
                            activebackground="#696969",
                            activeforeground="white",
                            bg="#636363",
                            command=self.__subtraction(),
                            fg="white",
                            font=("Arial", 15, "bold"),
                            relief="flat",
                            height=2,
                            width=5,
                            text="-"
                            )
        button_sub.pack(fill="both")
        button_sum = Button(button_frame_actions,
                            activebackground="#696969",
                            activeforeground="white",
                            bg="#636363",
                            command=self.__summation(),
                            fg="white",
                            font=("Arial", 15, "bold"),
                            relief="flat",
                            height=2,
                            width=5,
                            text="+"
                            )
        button_sum.pack(fill="both")

        button_more_actions = Button(button_frame_more_actions,
                                     activebackground="#1de9b6",
                                     bg="#1de9b6",
                                     command=self.__more_actions,
                                     relief="flat",
                                     height=22,
                                     width=5,
                                     )
        button_more_actions.pack(fill="both")


if __name__ == '__main__':
    root = Tk()
    # root.title("My calculator")
    # root.iconbitmap("icon.ico")
    # print(root.winfo_screenwidth())
    # print(root.winfo_screenheight())

    app = MyCalculator(master=root)
    app.mainloop()
