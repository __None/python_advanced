#  variant 10

# Task 1

list_task1 = ["ada", 15, 79, 212, 45, "rea", 15, 79, 45, "adskada", "srfsd", 1.5]

list_task1 = list(set(list_task1))
answer_task1 = list(map(str, list_task1))
answer_task1.sort()
print(answer_task1)

# Task 2

list_task2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 10, 12, 11, 13, "fgvkvg"]


def delete_even(some_list):
    iter_task2 = iter(some_list.copy())
    new_list = []
    for item in iter_task2:
        if str(item).isdigit():
            if not item % 2 == 0:
                new_list.append(item)
        else:
            new_list.append(item)
    return new_list


answer_var1_task2 = delete_even(list_task2)
print(answer_var1_task2)


# def is_not_even(some_number):
#     if not some_number % 2 == 0:
#         return True
#
#
# answer_var2_task2 = list(filter(is_not_even, list_task2))
# print(answer_var2_task2)

# Task 3


class SomeStrangeClass:

    def __enter__(self):
        print("I started doing something weird!")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("I stopped doing something weird!")

    def some_method(self):
        print("Some action")


with SomeStrangeClass() as some_obj:
    some_obj.some_method()
