# Tasks from the fourth lecture (slide number 7)
# Task 1
input_task1 = input("Enter the number: ")
print("Enter the number please") if not input_task1.isdigit() else None

# Task 2
sentence_task2 = input("Enter your sentence: ")
word_number = len(sentence_task2.split(" "))
print(word_number)

# Task 3
input_task3 = input("Enter your float: ")
fractional_part = float(input_task3) % 1
print(f"Fractional part of a number {input_task3} is {fractional_part}")

# Task 4
input_task4 = input("Enter your string: ")
new_str_task4 = input_task4.replace("a", "0")
print(new_str_task4)
