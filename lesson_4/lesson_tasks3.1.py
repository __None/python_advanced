# Tasks from the fourth lecture (slide number 5)
# Task 1
list_task1 = [0, "a", 13, "py", 666]
dict_task1 = dict.fromkeys(list_task1, 0)
print(dict_task1)

# Task 2
list_task2 = [{"name": "John", "age": 26}, {"name": "Elon", "age": 48},
              {"name": "Nikola", "age": 86}, {"name": "Jack", "age": 40}]

name_list_task2 = [x.setdefault("name") for x in list_task2]
age_list_task2 = [x.setdefault("age") for x in list_task2]

# Task 3
first_name_task3 = input("Enter first name: ")
last_name_task3 = input("Enter last name: ")
middle_name_task3 = input("Enter middle name: ")

dict_task3 = {"first_name": first_name_task3, "last_name": last_name_task3, "middle_name": middle_name_task3}
list_task3 = [{"first_name": "John", "last_name": "Smith", "middle_name": "Olegovich"},
              {"first_name": "Elon", "last_name": "Musk", "middle_name": "Asrorovich"},
              {"first_name": "Test", "last_name": "Test", "middle_name": "Test"}]

list_task3.append(dict_task3) if not list_task3.count(dict_task3) else None

# Task 4
list_task4 = [{"name": "John", "age": 26}, {"name": "Elon", "age": 48},
              {"name": "Nikola", "age": 86}, {"name": "Jack", "age": 40}]
oldest_task4 = list_task4[0]

for human in list_task4:
    if human["age"] > oldest_task4["age"]:
        oldest_task4 = human

print(oldest_task4["name"])
