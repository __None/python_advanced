# even = [x for x in range(1000)]
# print(even)

even = list(range(1000))
print([x for x in even if x % 2 == 0])

a = 10
b = 40
c = 50 if a > b else None


f = 20 and 15
print(c)
