# while and for
import time

i = 0
while True:
    print(i)
    time.sleep(1)
    i += 1
else:
    print("Break not happened")

# for key, value in my_dict.item():
#     print(f"{key}: {value}")

