# Task 1
text_task1 = """Lorem Ipsum is simply dummy text of the printing and typesetting 
industry. Lorem Ipsum has been the industry's standard dummy 
text ever since the 1500s, when an unknown printer took a galley 
of type and scrambled it to make a type specimen book. It has 
survived not only five centuries, but also the leap into electronic 
typesetting, remaining essentially unchanged. It was popularised in 
the 1960s with the release of Letraset sheets containing Lorem 
Ipsum passages, and more recently with desktop publishing 
software like Aldus PageMaker including versions of Lorem Ipsum."""  # 91; 4

# print(f"Number of words is: {len(text_task1.split(' '))}")
# print(f"Number of sentences is: {len(text_task1.split('.')) - 1}")

num_word_task1 = 0
num_sentence_task1 = 0

for index in text_task1:
    if index == " ":
        num_word_task1 += 1
    elif index == "." or index == "?" or index == "!":
        num_sentence_task1 += 1

print(f"Number of words is: {num_word_task1 + 1}")
print(f"Number of sentences is: {num_sentence_task1}")

# Task 2
while True:
    value_task2 = input("Enter your number for task 2: ")

    if value_task2.isdigit():
        if int(value_task2) % 2 == 0:
            print("Even")
            break
        else:
            print("Odd")
            break
    else:
        print("Try again! It must be a number")

# Task 3
some_value_task3 = input("Enter some values separated by commas: ")

if some_value_task3.replace(",", "").isdigit():
    some_list_task3 = [float(x) for x in some_value_task3.split(",")]
elif some_value_task3.replace(", ", "").isdigit():
    some_list_task3 = [float(x) for x in some_value_task3.split(", ")]
else:
    some_list_task3 = some_value_task3.split(",")

if len(some_list_task3) < 10:
    print(sorted(some_list_task3, reverse=True))
elif len(some_list_task3) > 10:
    print(sorted(some_list_task3))
else:
    print("List exactly 10 elements. No actions required")

# Task 4
while True:
    first_number_task4 = input("Enter first number: ")
    second_number_task4 = input("Enter second number: ")

    if first_number_task4.isdigit() and second_number_task4.isdigit():
        first_int_task4 = int(first_number_task4)
        second_int_task4 = int(second_number_task4)

        if first_int_task4 > second_int_task4:
            print(f"Dif is {first_int_task4 - second_int_task4}")
            break
        elif first_int_task4 < second_int_task4:
            print(f"Sum is {first_int_task4 + second_int_task4}")
            break
        else:
            print(f"Square is {pow(first_int_task4, second_int_task4)}")
            break
    else:
        print("Try again! It must be a number")

# Task 5
print("2a - 8b / (a - b + c)")

while True:
    a_task5 = input("Enter your value for 'a': ")
    b_task5 = input("Enter your value for 'b': ")
    c_task5 = input("Enter your value for 'c': ")

    if a_task5.isdigit() and b_task5.isdigit() and c_task5.isdigit():
        a_num_task5 = int(a_task5)
        b_num_task5 = int(b_task5)
        c_num_task5 = int(c_task5)

        print(2 * a_num_task5 - 8 * b_num_task5 / (a_num_task5 - b_num_task5 + c_num_task5))
        break
    else:
        print("Enter the number please")

# Task 6
list_task6 = [1, 5, 6, 3, 3, 2, 6, 8, 7, 256, 13, 666, 888, 0]
even_number_task6 = 0

for value in list_task6:
    if value % 2 == 0:
        even_number_task6 += 1

print(even_number_task6)

# Task 7
list_task7 = [1, 5, 6, 3, 3, 2, 6, 8, 7, 256, 13, 666, 888, 0]

for i in range(len(list_task7)):
    for j in range(len(list_task7) - 1 - i):
        if list_task7[j] > list_task7[j + 1]:
            list_task7[j], list_task7[j + 1] = list_task7[j + 1], list_task7[j]

print(list_task7)

# Task 8
fibonacci_task8 = [0, 1]

while True:
    length_task8 = input("Enter the length of the Fibonacci series for task8: ")

    if length_task8.isdigit():
        for index in range(int(length_task8)):
            fibonacci_task8.append(fibonacci_task8[index] + fibonacci_task8[index + 1])
        print(fibonacci_task8[0:int(length_task8)])  # If anyone wants to see zero elements
        break
    else:
        print("I asked for an integer (only number)")

# Task 9
while True:  # try 2147483647 XD on my computer, the calculation will take about 60 hours
    some_value_task9 = input("Enter your value (it must be a number): ")

    if some_value_task9.isdigit():
        some_number_task9 = int(some_value_task9)
        counter_task9 = 0

        for numbers in range(1, some_number_task9 + 1):
            if some_number_task9 % numbers == 0:
                counter_task9 += 1
        if counter_task9 <= 2:
            print(f"{some_number_task9} is simple!")
        else:
            print(f"{some_number_task9} is difficult!")
        break
