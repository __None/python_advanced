my_dict = {"name": "Loki", "age": "28", "cats": ["Tor", "Magnus"],
           1: "Some",
           1.2: "Hope",
           frozenset([1, 3]): "Some more",
           # class how key in dict
           True: "16"
           }  # ключами могут быть только хэшабильные типами
# хєшабильный - не изменяюмые типы данных
# print(my_dict["cats"][0])

# print(pprint.pprint(my_dict)) need lib

fromkeys_dict = dict.fromkeys(["Loki", "age"], 0)

# print(fromkeys_dict)
zip_dict = zip(["name", "Loki"], ["age", "28"])
print(dict(zip_dict))

# if class have't method don't use it, use dict

