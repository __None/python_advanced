import time
from functools import reduce


def some_method(x):
    if x % 2 == 0:
        return x
    return x + 1


if __name__ == '__main__':
    some_list = [1, 2, 3, 4, 5]

    # some_list = [x + 1 for x in some_list]

    # some_list = map(some_method, some_list)
    #
    # print(list(some_list))

    # some_list = filter(lambda x: x % 2 == 0, some_list)
    #
    # def even(x):
    #     return x % 2 == 0
    #
    # some_list = filter(even, some_list)
    #
    # print(list(some_list))

    # value = reduce(lambda x, y: x + y, some_list)
    # print(value)
    # print(sum(some_list))


class Cat:
    name = None
    color = None

    def __init__(self, name, color=None):
        self.name = name
        self.color = color or 'Black'
        # if color:
        #     self.color = color

    def __del__(self):
        print(f'Cat {self.name} dead now')

    def __str__(self):
        return f"{self.name} {self.color}"

    # def __unicode__(self):

    # def __int__(self):
    # def __float__(self):
    # def __len__(self):

    def _protected(self):
        pass

    def __private(self):
        pass

    def walk(self):
        print(f"{self.name} is walking now")


class MegaCat(Cat):
    age = 22

    def __init__(self, *args, **kwargs):
        self.age = kwargs.pop('age', 0)
        super().__init__(*args, **kwargs)

    def walk(self):
        super().walk()
        print(f'{self.name} walk as cool cat!')


class Bird:
    name = 'Kesha'

    def fly(self):
        print(f"{self.name} is flight now!")

    def walk(self):
        print(f"{self.name} walk as a bird now!")


class SwimMixin:

    def swim(self):
        print(f'{self.name} swimming now!')


class CatBird(Bird, SwimMixin, Cat):

    def walk(self):
        super(Bird, self).walk()

    def __del__(self):
        pass


catbird = CatBird(name='Fenrir', color='Red')

print(CatBird.__mro__)


class Player(CatBird):
    strength = 20
    damage = 0

    @property
    def health(self):
        return self.strength * 10 - self.damage

    @health.setter
    def health(self, value):
        self.damage = value

    @health.deleter
    def health(self):
        self.damage = self.health

    def walk(self):
        print('Walking')

    @staticmethod
    def jump():
        print('Jump')

    @classmethod
    def shoot(cls):
        cls.health = 50
        print(cls.health)
        print('Shooting')

    @classmethod
    def boost(cls, value):
        cls.health = value

    # def __new__(cls, *args, **kwargs):

    def fly(self):
        print('Fly')


# Player.jump()
# Player.boost(50)
player = Player(name='Benya')
player.health = 50
del player.health
print(player.health)
# player.jump()
# Player.shoot()


class A:
    test = 1

    def __init__(self, test):
        self.test = test

    def some(self):
        pass


class B:

    ro = 2

    def __int__(self, ro):
        self.ro = ro

    def main(self):
        pass
    

class C(A, B):

    def __init__(self, *args, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def some(self):
        super(C, self).some()


c = C(test=1, ro=1, asd=1)

print(c.test, c.ro)

# from calendar import isleap
# import calendar
#
# print(getattr(calendar, 'isleap')(2222))


# player = Player(name='Benya')
#
# delattr(player, 'health')

# del player.health

# delattr(player, 'name')

# print(player.health)

# player.health = 20
# value = getattr(player, 'health', None)
# if value:
#     print('some crazy logic here')
# print()
# print(isinstance(player, SwimMixin))
# while True:
#     action = input('Make a move! ')
#     if hasattr(player, action):
#         getattr(player, action)()
#         if action == 'jump':
#             player.jump()
#         elif action == 'walk':
#             player.walk()
#         elif action == 'shoot':
#             player.shoot()

# catbird.walk()
# catbird.fly()
# catbird.swim()
# cat1 = Cat(name='Tor', color='Black')
# del cat1
# time.sleep(10)
# cat1._protected()
# cat1._Cat__private()
# print(dir(cat1))

# cat2 = Cat(name='Izum', color='Ginger')
# cat3 = MegaCat(name='Maguns', color='White', age=34)

# cat1.walk()
# cat3.walk()

# print(cat3.age)

# cat1 = Cat()
# cat2 = Cat()

# cat1.legs_count = 3

# cat1_str = str(cat1)
# print(cat1_str)
# print(cat1, cat2)
# print(cat1.legs_count)
# cat1.walk()
# print(type(cat1))
