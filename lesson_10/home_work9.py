from operator import itemgetter


class HomeLibrary:

    __books_list = []

    def search_books(self, search_parameter):
        return_books = []
        books_iter = iter(self.__books_list)
        for book in books_iter:
            parameter_list = book.values()
            for values in parameter_list:
                if values == search_parameter:
                    return_books.append(book)
        return return_books

    def sort_books(self, sort_key, reverse=False):
        self.__books_list.sort(key=itemgetter(sort_key), reverse=reverse)

    def add_book(self, title, author="unknown", genre="unknown", year="unknown", pages_number="unknown", **kwargs):
        check_book = self.search_books(title)
        if check_book:
            print("Book with such title already exists!")
        else:
            book = {"title": title, "author": author, "year": year, "pages_number": pages_number, "genre": genre}
            book.update(kwargs)
            self.__books_list.append(book)

    def remove_books(self, remove_parameter=None):
        if remove_parameter:
            remove_books = self.search_books(remove_parameter)
            remove_books_iter = iter(remove_books)
            for book in remove_books_iter:
                self.__books_list.remove(book)
        else:
            self.__books_list.clear()

    def __output_books(self, list_to_show):
        books_iter = iter(list_to_show)
        for book in books_iter:
            book_keys = book.keys()
            print("\n----Book from library!----")
            for key in book_keys:
                print(f"{key.capitalize()} is {book[key]}")

    def show_books(self, show_parameter=None):
        if show_parameter:
            books_for_show = self.search_books(show_parameter)
            self.__output_books(books_for_show)
        else:
            self.__output_books(self.__books_list)


librarian = HomeLibrary()

librarian.add_book(title="451 градус по Фаренгейту", author="Рэй Брэдбери", genre="Научная фантастика, антиутопия",
                   year="1953", pages_number="288")
librarian.add_book(title="Гарри Поттер", author="Джоан Роулинг", genre="Роман, Фэнтези", year="1997",
                   pages_number="455")
librarian.add_book(title="Зов Ктулху", author="Говард Лавкрафт", genre="Ужасы, Фэнтези", year="2017",
                   pages_number="416")
librarian.add_book(title="Гарри Поттер", author="Джоан Роулинг", genre="Роман, Фэнтези", year="1997",
                   pages_number="455")

print(librarian.search_books('Гарри Поттер'))
# librarian.remove_books()
librarian.sort_books("title")
librarian.show_books()
