# Tasks from the third lecture (at the end of the slides)
# Task 1
list_task1 = [8, 1, 3, 5, 6, 8, 9, 3, 3, 8]
print(pow(list_task1[0], list_task1[-1]))

# Task 2
str_task2 = input("Enter your string for task 2: ")
char_task2 = input("Enter your char for task 2: ")

print(str_task2.count(char_task2))

# Task 3
str_task3 = "I have a pen I have an apple."
count_task3 = len(set(str_task3))

print(count_task3)

# Task 4
list_task4 = [8, 1, 3, 5, 6, 8, 9, 3, 3, 8]
first_sum = list_task4[0] + list_task4[1]
last_sum = list_task4[-1] + list_task4[-2]

print((first_sum, last_sum))

# Task 5
list_task5 = [8, 1, 3, 5, 6, 8, 9, 3, 3, 8]
list_task5.append(64)
list_task5.pop(0)

print(list_task5)

# Task 6 sorry, I did not understand how to solve this problem without cycles
list_task6 = [6, 4, 3, 1, 8, 7, 5, 5, 0, 0, 6]
set_task6 = set(list_task6.copy())
answer_list_task6 = [x for x in set_task6 if list_task6.count(x) > 1]

print(answer_list_task6)

# Task 7
first_list_task7 = [6, 4, 3, 1, 8, 7, 5]
second_list_task7 = [0, 9, 2, 5, 3, 7, 8]

print(set(first_list_task7).intersection(set(second_list_task7)))

# Task 8
str_task8 = "12345678900987654321"
list_task8 = list(str_task8)
list_task8.sort()
print(f"Min is {list_task8[0]}, max is {list_task8[-1]}")
