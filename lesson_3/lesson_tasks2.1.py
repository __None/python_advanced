# Task 1
str_task1 = input("Enter your string for task 1: ")
print(str_task1.isdigit())

# Task 2
str_task2 = input("Enter your string for task 2: ")
print(str_task2.upper())

# Task 3
str_task3 = input("Enter your string for task 3: ")
print(str_task3.title())

# Task 4
str_task4 = input("Enter your string for task 4: ")
print(str_task4.replace(" ", ""))

# Task 5
str_task5 = input("Enter your string for task 5: ")
replaceable_char_task5 = input("Enter char you want to replace: ")
replacing_char_task5 = input("Enter a replacement char: ")

print(str_task5.replace(replaceable_char_task5, replacing_char_task5))

# Task 6
str_task6 = input("Enter your string for task 6: ")
print(str_task6.swapcase())

# Task 7
str_task7 = input("Enter your string for task 7: ")
print(str_task7.strip("_"))
