username = "Name"
age = 22

# print("Hi" + username + "! Your age is " + str(age))

# print("Hi %s! Your age is %i." % (username, age))

# print("Hi {}! Your age is {}.".format(username, age))

# print("Hi {0}! Your age is {1}.".format(username, age))  # No mix

# print("Hi {name}! Your age is {age}.".format(name=username, age=age))

print(f"Hi {username.upper()}! Your age is {age + 20}.")  # f-str is greatest, only python 3.5 <




