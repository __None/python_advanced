# sql-ex.ru

text = "Some str. With some random words in it."

# print(text.find("."))
# print(text.rfind(".", 10, 21))

# print(text.index("_"))

# print(text.replace(".", ","))

# print(len(text.split(" ")))

# print(text.isdigit())  # if str num

# print(text.isalpha())  # if str only alphavit

# print("qwertyuio".islower())  # True if lower register

# print("ASDFGHJ".isupper())  # True if

# print("     \n".isspace())

# print("Some Str Is Great".istitle())

# print(text.upper())  # All char is str in upper register

# print(text.startswith("Some"))

# print(text.endswith("."))

# print("-".join(["1", "2", "3"]))

# print("qwertyuio dfghjk zxcvbnm".capitalize())

# print("!", "test".center(100, "_"), "!", sep="")

# print(text.count(" ") + 1)

# print("!", "\t".expandtabs(4), "!", sep="")

# print("!", "        ertyuio     ".lstrip().rstrip(), "!", sep="")
# print("!", "_____ertyuio______".strip("_"), "!", sep="")

# print(text.partition("random"))

# print(text.swapcase())

# print(text.title())

# print("test".zfill(100))

# print("test".ljust(100, "_"))
# print("test".rjust(100, "_"))

# print(dir(text))

help(str)
