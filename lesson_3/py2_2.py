text = "Some str. With some random words in it."

if __name__ == '__main__':
    # print(text.replace(' ', ''))
    # print(text.split('.'))
    # print(text.isdigit())
    # print("1.23".isdigit())
    # print(text.isalpha())
    # print(text.isalnum())
    # print("123".isdecimal())
    # print("...w...".islower())
    # print("...W...".isupper())
    # print("\n".isspace())
    # print("Some Str".istitle())

    # print(text.upper())
    # print(text.lower().split('some'))
    # print(text.startswith('Some'))
    # print(text.endswith('it.'))
    # print(' '.join(['1', '2', '3']))
    # print("qweqwe qwe qwe".capitalize())
    # print('!', "test".center(100, '_'), '!', sep="")
    # print(text.count(' ') + 1)

    # first = 1
    # second = 2

    # first, second = second, first

    # print('!', "\t".expandtabs(0), '!', sep="")
    # print('!', "______sadasd______".strip('_'), '!', sep="")
    # print(text.partition('random'))
    # print(text.swapcase())
    # print(text.title())
    # print('test'.zfill(3))
    # print("test".ljust(100, '_'))
    # print("test".rjust(100, '_'))
    # print(dir(text))
    # help(str)
    username = 'Loki'
    age = 29

    # print('Hi ' + username + "! Your age is " + str(age))
    # print("Hi {name}! Your age is {age}".format(
    #     name=username, age=age
    # ))
    # print(f"Hi {username}! your age is {age}")

    # matrix = [
    #     [1, 2, 3],
    #     [2, 3, 1],
    #     [2, 5, 2]
    # ]

    # print(list(text))
    my_list = [1, 3, 4, 2, 1, "asd", True, None, [1, 2, 3]]
    # my_list.append('my new element')
    # my_list.extend([1, 2, 3])
    # my_list.insert(4, 'New')
    # my_list.remove(1)
    # my_list[4] = 123
    # print(my_list.pop(5))
    # print(my_list.index(1))
    # my_next_list = [1, 2, 2, 3, 1, 4, 7, 3, 2]
    # my_next_list.sort()
    # my_list_str = ['asd', 'sadasd', 'asd', 'qwe', 'asdasdasd']
    # my_list_str.sort()
    # a = [1, 2, 3]
    # b = a.copy()
    # a[0] = 12
    # print(b)
    # my_tuple = (1, 2, 3, 4)
    # my_tuple = tuple([1, 3])
    # my_tuple = (1, )
    # print(my_tuple)
    # print(my_list[8][1])
    # my_list[0] = 23
    # print(my_list)
    # print(my_list[len(my_list) - 1])
    # print(my_list[-1])
    # text[0] = 'q'
    # print(text[0])

    my_set = {1, 1, 2, 3, 2, 5}
    print(my_set)

    # my_list = [1, 2, 1, 34, 32, 2, 1, 34]
    # print(list(set(my_list)))
