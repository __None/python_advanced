# Task 1
value_task1 = float(input("Enter your value for task 1: "))

print(f"{value_task1} rounded to 0 digits is {round(value_task1, 0)}!")
print(f"{value_task1} rounded to 2 digits is {round(value_task1, 2)}!")
print(f"{value_task1} rounded to 5 digits is {round(value_task1, 5)}!")

# Task 2
list_task2 = [8, 5, 4, "a", "Hope", 4.6, "str", [1, 2, 3.4], 6.66]
print(f"I ({list_task2[len(list_task2) - 1]}) am the last!")

# Task 3
a = int(input("Enter your 'a' value for task 3: "))
b = int(input("Enter your 'b' value for task 3: "))
c = int(input("Enter your 'c' value for task 3: "))
formula_task3 = input("Enter your formula for task 3: ")

print(f"Result of your formula is {eval(formula_task3)}!")

# Task 4
list_task4 = [8, 5, 4, "a", "Hope", 4.6, "str", [1, 2, 3.4], 6.66]

begin_task4 = int(input("Enter the beginning of the range: "))
end_task4 = int(input("Enter the end of the range: "))

print(f"The list in the range from {begin_task4} to {end_task4} is {list_task4[begin_task4:end_task4]}")

# Task 5
str_task5 = input("Enter your string for task 5: ")
print(list(set(str_task5)))

# Task 6
first_set_task6 = {6, 4, 3, 1, 8, 7, 5}
second_set_task6 = {0, 9, 2, 5, 3, 7, 8}

intersection_notset = list(first_set_task6.intersection(second_set_task6))
print(intersection_notset[0] - intersection_notset[-1])

# Task 7
first_list_task7 = [8, 5, 4, "a", "Hope", 4.6, "str", 6.66]
second_list_task7 = [6, 4, 3, 1, 8, 7, 5, 0, 9, 2, 5, 3, 7, 8, "a", 8.88]

new_list_task7 = first_list_task7.copy()
new_list_task7.extend(second_list_task7)

print(f"Result of the seventh task is {list(set(new_list_task7))}")

# Task 8
tuple_task8 = (6, 4, 3, 1, 8, 7, 5)
list_task8 = [0, 9, 2, 5, 3, 7, 8]

answer_task8 = set(tuple_task8).intersection(set(list_task8))
print(f"Result of the eighth task is {answer_task8}")
