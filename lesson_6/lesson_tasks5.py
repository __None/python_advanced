# Tasks from the sixth lecture (slide number 16)
# Task 1 Написать функцию которая принимает строку и возвращает True если введено число (int, float)

#
# def func_task1(some_str_task1):
#     if some_str_task1.isdigit():
#         return True
#     elif float(some_str_task1):
#         return True
#     else:
#         return False
#
#
# func_task1("26")

# Task 2


# def func_task2(first_num, second_num):
#     if first_num % second_num == 0:
#         return True
#     else:
#         return False


# try:
#     func_out_task2 = func_task2(27, 3)
#     print(func_out_task2)
# except ZeroDivisionError:
#     print("False")

# Task 3 Написать функцию которая принимает любое количество аргументов и сортирует их


def func_task3(*args):
    args_list = [*args]
    iter_args = iter(args_list)
    str_args = []
    numeric_arg = []
    for arg in iter_args:
        numeric_arg.append(arg) if str(arg).isnumeric() else str_args.append(arg)
    print(str_args)
    print(numeric_arg)
    str_args.sort()
    numeric_arg.sort()
    numeric_arg.extend(str_args)
    return numeric_arg


func_out_task3 = func_task3(1, 89, 8, 56, 91, 3, 2, 7, "a", "pop", 45)
print(func_out_task3)
