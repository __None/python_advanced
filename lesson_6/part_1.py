def add(x: int, y: int) -> int:
    """
    Sum two numbers

    :param x: int first number
    :param y: int second number
    :return: int result of sum two numbers
    """
    c = x + y
    return c


def _protected():
    pass


def __private():
    pass


def increase(some_list):
    for x in some_list:
        x += 1


my_list = [1, 1, 1, 1, 1, 1]
print(my_list)


def division(x, y):  # if more then 5 param div it func
    return x / y


# c = division(2, 0)
c = division(y=2, x=0)  # we can mix it
print(c)


def swap(x, y=0):
    x, y = y, x
    if y:
        # return y / x
        pass
    return x + y


run = swap(6)
print(run)


def my_method(a=[]):
    a.append(1)
    print(a)


my_method()
my_method()
my_method()


def sort_args(x, y, *args, **kwargs):
    print(x)
    print(y)
    print(args)
    print(kwargs)


sort_args(15, 12, 21, 13, 55, 11, quota=56, hope=99)

a, *b, c = (1, 2, 3, 4, 6, 6)
