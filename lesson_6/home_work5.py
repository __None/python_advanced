# Task 1

ratio_numerals = (("M", 1000), ("CM", 900), ("D", 500), ("CD", 400), ("C", 100), ("XC", 90),
                  ("L", 50), ("XL", 40), ("X", 10), ("IX", 9), ("V", 5), ("IV", 4), ("I", 1))


def conversion_ar(arabic_number: int) -> str:

    roman_number = ""
    for tuple_index in ratio_numerals:
        div_integer = arabic_number // tuple_index[1]
        if div_integer:
            roman_number += tuple_index[0] * div_integer
            arabic_number -= tuple_index[1] * div_integer
    return roman_number


def conversion_ra(roman_number: str) -> int:
    dict_ratio = dict(ratio_numerals)
    arabic_number = 0
    indicator = False
    for index in range(len(roman_number) - 1):
        if not indicator:
            value_roman = dict_ratio.setdefault(roman_number[index])
            next_value_roman = dict_ratio.setdefault(roman_number[index + 1])
            if value_roman < next_value_roman:
                arabic_number += dict_ratio.setdefault(roman_number[index] + roman_number[index + 1])
                indicator = True
            else:
                arabic_number += value_roman
        else:
            indicator = False
    arabic_number += dict_ratio.setdefault(roman_number[-1]) if not indicator else 0
    return arabic_number


def check_arabic() -> int:
    while True:
        arabic_number = input("Enter an Arabic number using integer (from 1 to 3999): ")
        if arabic_number.isdigit():
            arabic_int = int(arabic_number)
            if 0 < arabic_int < 4000:
                return arabic_int


def check_roman() -> str:
    roman_value = "MDCLXVI"
    while True:
        roman_number = input("Enter a Roman number using the characters on the keyboard: ")
        roman_iter = iter(roman_number)
        flag = True
        for char in roman_iter:
            if not roman_value.count(char):
                flag = False
        if flag is True:
            return roman_number


def conversion_program():
    program_mode = input("""What mode of operation do you want? From Roman to Arabic (0), 
    from Arabic to Roman (1), else - exit the program: """)
    if program_mode == "0":

        roman_number = check_roman()
        result = conversion_ra(roman_number)
        print(result)
    elif program_mode == "1":
        pass
        arabic_number = check_arabic()
        result = conversion_ar(arabic_number)
        print(result)
    else:
        print("Goodbye")


# Task 2


def no_numbers(file_path):  # I didn't make the defense in the hope that the functions would be used by prudent people;)
    # THE FIRST SOLUTION

    # with open(file_path, "r+") as working_file:
    #     file_contents = working_file.read()
    #     counter = 0
    #     file_iter = iter(file_contents)
    #     for char in file_iter:
    #         if char.isdigit():
    #             file_contents = file_contents.replace(char, "")
    #             counter += 1
    #     working_file.seek(0)
    #     print(file_contents)
    #     working_file.write(file_contents + " " * counter)

    # THE SECOND SOLUTION

    with open(file_path, "r+") as working_file:
        file_contents = working_file.read()
        counter = 0
        for index in range(10):
            while_counter = file_contents.count(str(index))
            counter += while_counter
            if while_counter:
                file_contents = file_contents.replace(str(index), "")
        working_file.seek(0)
        print(file_contents)
        working_file.write(file_contents + " " * counter)


# Task 3

def encryption_caesar_check(int_check):
    var_type = type(int_check)
    str_type = str(var_type).split("'")
    if str_type[1] == "int":
        return int_check
    else:
        print(f"Hey! You used {str_type[1]}! Please use int!")
        print("I replaced it with zero!")
        return 0


def encryption_caesar(text="Eat more of those soft French rolls and have some tea.", shift=0) -> str:
    shift = encryption_caesar_check(shift)
    latin_alphabet = "abcdefghijklmnopqrstuvwxyz"  # 26
    text_iter = iter(text)
    encryption_list = []
    encryption_str = ""

    for char in text_iter:
        if char.isalpha():
            if char.isupper():
                up_to_low = char.lower()
                new_index = (latin_alphabet.index(up_to_low) + shift) % 26
                encryption_list.append(latin_alphabet[new_index].upper())
            else:
                new_index = (latin_alphabet.index(char) + shift) % 26
                encryption_list.append(latin_alphabet[new_index])
        elif char.isdigit():
            encryption_list.append(str((int(char) + shift) % 9))
        else:
            encryption_list.append(char)

    for encryption_char in encryption_list:
        encryption_str += encryption_char

    return encryption_str


# Task 4


def decoder_caesar(text="Eat more of those soft French rolls and have some tea.", shift=0) -> str:
    shift *= -1
    decoder_str = encryption_caesar(text, shift)
    return decoder_str


# Task 5
# The hope is still alive! And now seriously, I do not know how to check the path to the file without the library :(
def file_caesar(file_path, shift=0):
    with open(file_path, "r") as open_file:
        open_str = open_file.read()
        with open("encrypted_file.txt", "w") as encrypted_file:
            encrypted_file.writelines(encryption_caesar(open_str, shift))
