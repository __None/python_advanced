# # В директории лежат входные текстовые файлы, проименованные следующим образом: in_<N>.dat, где N - натуральное число.
# # Каждый файл состоит из двух строк.
# # В первой строке - число, обозначающее действие, а во второй - числа с плавающей точкой, разделенные пробелом.
# # Действия могут быть следующими:
# # 1 - сложение
# # 2 - умножение
# # 3 - сумма квадратов
# # Необходимо написать асинхронное приложение, которое выполнит требуемые действия над числами
# # и сумму результатов запишет в файл out.dat.
from os.path import isfile
from re import search
from time import time


def calculate_action(actions, value):
    value_list = value.split()
    first_value = float(value_list[0])
    second_value = float(value_list[1])
    data_found = search(r"\d+", actions)
    actions = data_found[0] if data_found else 0
    if actions == "1":
        return first_value + second_value
    elif actions == "2":
        return first_value * second_value
    elif actions == "3":
        return first_value ** 2 + second_value ** 2
    else:
        return 0


def main():
    out_results = 0
    n_counter = 1
    while True:
        file_path = f"in_{n_counter}.dat"
        if isfile(file_path):
            with open(file_path, "r") as dat_file:
                dat_file_list = dat_file.readlines()
                # print(dat_file_list)
                n_counter += 1
                out_results += calculate_action(dat_file_list[0], dat_file_list[1])
                # print(out_results)
        else:
            break
    with open("out.dat", "w") as out_dat:
        out_dat.write(str(out_results))


if __name__ == '__main__':

    time1 = time()
    main()
    print(time() - time1)  # 0.0045
