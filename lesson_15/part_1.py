from time import time
from requests import get
from multiprocessing import Process


def fetch_data(item):
    print(item)
    response = get("https://loremflickr.com/320/240")
    if response.status_code == 200:
        # print(dir(response))
        # print(response.content)
        with open(f"images_{item}.jpg", "wb") as img:
            img.write(response.content)


count = input("Enter image number: ")
time1 = time()
if count.isdigit():
    count = int(count)
    for item in range(count):
        process = Process(target=fetch_data)
    print(time() - time1)
