import time
import asyncio
import aiohttp

from requests import get
from multiprocessing import Process


# def fetch_data(item):
#     print(item)
#     response = get('https://loremflickr.com/320/240')
#     if response.status_code == 200:
#         with open(f'images/img_{item + 1}.jpg', 'wb') as img:
#             img.write(response.content)

async def fetch_data(items_range, session):
    for item in range(*items_range):
        async with session.get('http://loremflickr.com/320/240',
                               allow_redirects=True, verify_ssl=False) as response:
            data = await response.read()
            with open(f'images/img_{item + 1}.jpg', 'wb') as img:
                img.write(data)


async def main(count):
    tasks = []
    prev_page = 0
    async with aiohttp.ClientSession() as session:
        for item in range(0, count+10, 10):
            tasks.append(
                asyncio.ensure_future(fetch_data([prev_page, item], session))
            )
            prev_page = item
        await asyncio.gather(*tasks)

if __name__ == '__main__':
    count = input('Enter images count: ')

    if count.isdigit():
        time1 = time.time()
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main(int(count)))
        print(time.time() - time1)

    # time1 = time.time()
    # if count.isdigit():
    #     count = int(count)
    #     for item in range(count):
    #         process = Process(target=fetch_data, args=(item, ))
    #         process.start()
    #
    # print(time.time() - time1)
    # async def counter():
    #     item = 1
    #     while True:
    #         print(item)
    #         await asyncio.sleep(1)
    #         item += 1
    #         if item == 10:
    #             break
    #
    # async def counter5():
    #     item = 1
    #     while True:
    #         if item % 5 == 0:
    #             print(f'{item} new gen!')
    #         await asyncio.sleep(1)
    #         item += 1
    #         if item == 21:
    #             break
    #
    # async def main():
    #     task = asyncio.ensure_future(counter())
    #     task1 = asyncio.ensure_future(counter5())
    #     await asyncio.gather(*[task, task1])
    #
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(main())
