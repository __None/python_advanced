from time import time
from asyncio import ensure_future, gather, get_event_loop
from aiohttp import ClientSession


async def fetch_data(item, session):
    print(item)
    async with session.get("https://loremflickr.com/320/240", allow_redirects=True) as response:
        data = await response.read()
        with open(f"image_{item + 1}.jpg", "wb") as img:
            img.write(data)


async def main(count):
    tasks = []
    async with ClientSession() as session:
        for item in range(count):
            tasks.append(ensure_future(fetch_data(item, session)))
            await gather(*tasks)


count = input("Enter image number: ")

if count.isdigit():
    time1 = time()
    count = int(count)
    loop = get_event_loop()
    loop.run_until_complete(main(count))
    print(time() - time1)
