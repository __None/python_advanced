from asyncio import ensure_future, gather, get_event_loop, sleep


async def counter():
    item = 1
    while True:
        print(item)
        await sleep(1)
        item += 1
        if item == 10:
            break


async def counter5():
    item = 1
    while True:
        if item % 5 == 0:
            print(f"{item} next gen")
        await sleep(1)
        item += 1
        if item == 21:
            break


async def main():
    task = ensure_future(counter())
    task1 = ensure_future(counter5())
    await gather(*[task, task1])


loop = get_event_loop()
loop.run_until_complete(main())
