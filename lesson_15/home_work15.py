from time import time
from os.path import isfile
from re import search
from asyncio import ensure_future, gather, get_event_loop

out_results = 0


async def calculate_action(file_path):
    global out_results
    with open(file_path, "r") as dat_file:
        dat_file_list = dat_file.readlines()
        value_list = dat_file_list[1].split()
        first_value = float(value_list[0])
        second_value = float(value_list[1])
        data_found = search(r"\d+", dat_file_list[0])
        actions = data_found[0] if data_found else 0
        if actions == "1":
            out_results += (first_value + second_value)
        elif actions == "2":
            out_results += (first_value * second_value)
        elif actions == "3":
            out_results += (first_value ** 2 + second_value ** 2)
        else:
            out_results += 0


async def main():
    tasks = []
    n_counter = 1
    while True:
        file_path = f"in_{n_counter}.dat"
        if isfile(file_path):
            tasks.append(ensure_future(calculate_action(file_path)))
            n_counter += 1
        else:
            break
    await gather(*tasks)
    with open("out.dat", "w") as out_dat:
        out_dat.write(str(out_results))


if __name__ == '__main__':
    time1 = time()
    loop = get_event_loop()
    loop.run_until_complete(main())
    print(time() - time1)
